db_user=$(openssl rand -hex 20)
db_password=$(openssl rand -hex 32)
db_name=$(openssl rand -hex 20)

echo "# service:supertoken_db" > .env
echo "POSTGRES_USER=${db_user}" >> .env
echo "POSTGRES_PASSWORD=${db_password}" >> .env
echo "POSTGRES_DB=${db_name}" >> .env

echo "# service:supertokens" >> .env
echo "POSTGRESQL_CONNECTION_URI=postgresql://${db_user}:${db_password}@supertoken_db:5432/${db_name}" >> .env